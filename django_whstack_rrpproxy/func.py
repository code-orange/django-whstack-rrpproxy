import idna
import requests
import xmltodict
from currency_converter import CurrencyConverter
from django.conf import settings
from lxml import etree


def get_data_from_rrpproxy(xml_doc):
    xml_pretty_string = etree.tostring(
        xml_doc, pretty_print=True, xml_declaration=True, encoding="UTF-8"
    )
    session = requests.Session()
    session.headers = {
        "Content-Type": "text/xml",
    }
    request_args = {
        "url": settings.RRPPROXY_URL,
        "data": xml_pretty_string,
        "verify": False,
    }
    response = session.post(**request_args)
    xml_dict = xmltodict.parse(response.content)
    return xml_dict


def gen_xml_message(structs):
    xml_request = etree.Element("methodCall")
    xml_req_method_name = etree.Element("methodName")
    xml_req_method_name.text = "Api.xcall"
    xml_request.append(xml_req_method_name)
    xml_req_params = etree.Element("params")
    xml_req_param = etree.Element("param")
    xml_req_value = etree.Element("value")
    xml_req_struct = etree.Element("struct")

    for struct in structs:
        xml_req_struct.append(struct)

    xml_req_value.append(xml_req_struct)
    xml_req_param.append(xml_req_value)
    xml_req_params.append(xml_req_param)

    xml_request.append(xml_req_params)

    return xml_request


def gen_xml_member(member_name, member_value, member_value_type):
    xml_req_member = etree.Element("member")

    xml_req_member_name = etree.Element("name")
    xml_req_member_name.text = member_name
    xml_req_member.append(xml_req_member_name)

    xml_req_member_value = etree.Element("value")

    xml_req_member_value_type = etree.Element(member_value_type)
    xml_req_member_value_type.text = member_value

    xml_req_member_value.append(xml_req_member_value_type)

    xml_req_member.append(xml_req_member_value)

    return xml_req_member


def check_domain_availability(domain: str):
    struct_list = list()
    struct_list.append(gen_xml_member("s_login", settings.RRPPROXY_USER, "string"))
    struct_list.append(gen_xml_member("s_pw", settings.RRPPROXY_PASSWD, "string"))
    struct_list.append(gen_xml_member("command", "CheckDomain", "string"))
    struct_list.append(
        gen_xml_member("domain", idna.encode(domain).decode("utf-8"), "string")
    )

    xml_doc = gen_xml_message(struct_list)

    xml_dict = get_data_from_rrpproxy(xml_doc)

    response_members = xml_dict["methodResponse"]["params"]["param"]["value"]["struct"][
        "member"
    ]

    for member in response_members:
        if member["name"] == "CODE":
            if int(member["value"]["int"]) == 210:
                return True
            else:
                return False


def register_or_transfer_domain(domain: str, auth_code=None):
    struct_list = list()
    struct_list.append(gen_xml_member("s_login", settings.RRPPROXY_USER, "string"))
    struct_list.append(gen_xml_member("s_pw", settings.RRPPROXY_PASSWD, "string"))

    if auth_code is None:
        struct_list.append(gen_xml_member("command", "AddDomain", "string"))
        struct_list.append(gen_xml_member("ownercontact0", "P-KQO545", "string"))
        struct_list.append(gen_xml_member("admincontact0", "P-KQO545", "string"))
        struct_list.append(gen_xml_member("techcontact0", "P-KQO545", "string"))
        struct_list.append(gen_xml_member("billingcontact0", "P-KQO545", "string"))
    else:
        struct_list.append(gen_xml_member("command", "TransferDomain", "string"))
        struct_list.append(gen_xml_member("action", "REQUEST", "string"))
        struct_list.append(gen_xml_member("auth", auth_code, "string"))

    struct_list.append(
        gen_xml_member("domain", idna.encode(domain).decode("utf-8"), "string")
    )
    struct_list.append(gen_xml_member("nameserver0", "ns01.srvfarm.net", "string"))
    struct_list.append(gen_xml_member("nameserver1", "ns02.srvfarm.net", "string"))

    xml_doc = gen_xml_message(struct_list)

    xml_dict = get_data_from_rrpproxy(xml_doc)

    response_members = xml_dict["methodResponse"]["params"]["param"]["value"]["struct"][
        "member"
    ]

    for member in response_members:
        if member["name"] == "CODE":
            if str(int(member["value"]["int"])).startswith("5"):
                raise ValueError
            elif int(member["value"]["int"]) == 200:
                return True
            else:
                raise ValueError

    return False


def get_zone_list():
    struct_list = list()
    struct_list.append(gen_xml_member("s_login", settings.RRPPROXY_USER, "string"))
    struct_list.append(gen_xml_member("s_pw", settings.RRPPROXY_PASSWD, "string"))
    struct_list.append(gen_xml_member("command", "QueryZoneList", "string"))

    xml_doc = gen_xml_message(struct_list)

    xml_dict = get_data_from_rrpproxy(xml_doc)

    response_members = xml_dict["methodResponse"]["params"]["param"]["value"]["struct"][
        "member"
    ]

    zone_count = 0
    zones = dict()

    for member in response_members:
        if member["name"] == "PROPERTY":
            for properties in member["value"]["struct"]["member"]:
                if properties["name"] == "TOTAL":
                    zone_count = int(
                        properties["value"]["array"]["data"]["value"]["int"]
                    )

            for i in range(0, zone_count - 1):
                zones[str(i)] = dict()

                for properties in member["value"]["struct"]["member"]:
                    if properties["name"] not in (
                        "ZONE",
                        "3RDS",
                        "PERIODTYPE",
                        "SETUP",
                        "ANNUAL",
                        "TRANSFER",
                        "TRADE",
                        "RESTORE",
                        "APPLICATION",
                        "CURRENCY",
                    ):
                        continue

                    if isinstance(properties["value"]["array"]["data"]["value"], list):
                        zones[str(i)][properties["name"].lower()] = list(
                            properties["value"]["array"]["data"]["value"][i].values()
                        )[0]

    zones_clean = list()

    for zone_counter, zone in zones.items():
        zones_clean.append(zone)

    return zones_clean


def get_zone_prices(destination_currency: str = "EUR"):
    zone_price = dict()

    currency_conv = CurrencyConverter()

    for zone in get_zone_list():
        if zone["periodtype"] == "YEAR":
            zone_price[zone["zone"]] = dict()

            price_tags = (
                "annual",
                "restore",
                "setup",
                "transfer",
            )

            for price_tag in price_tags:
                if zone[price_tag] == "N/A":
                    zone_price[zone["zone"]][price_tag] = 0
                    continue

                zone_price[zone["zone"]][price_tag] = float(zone[price_tag])

                if zone["currency"] != destination_currency:
                    zone_price[zone["zone"]][price_tag] = currency_conv.convert(
                        zone_price[zone["zone"]][price_tag],
                        zone["currency"],
                        destination_currency,
                    )

    return zone_price

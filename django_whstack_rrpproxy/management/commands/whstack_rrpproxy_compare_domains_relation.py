import csv

from django.core.management.base import BaseCommand

from django_whstack_models.django_whstack_models.models import WhRegistryDomains


class Command(BaseCommand):
    help = "whstack_rrpproxy_compare_domains_relation"

    def handle(self, *args, **options):
        csv_file = open("data/domainlist.csv")
        csv_reader = csv.reader(csv_file, delimiter=";")

        # skip headers
        next(csv_reader)

        for row in csv_reader:
            # Check if Domain in known by WhStack
            try:
                domain_relation = WhRegistryDomains.objects.get(domain=row[0])
            except WhRegistryDomains.DoesNotExist:
                print("Domain not found in relations: " + row[0])
